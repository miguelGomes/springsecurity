package com.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.MessageFormat;

@Controller
@RequestMapping("/secure")
public class SecurityHelloController {

    @RequestMapping("/hello/{name}")
    @ResponseBody
    public String hello(@PathVariable("name") String name) {
        return MessageFormat.format("Hello {0} from secure endpoint", name);
    }

}
